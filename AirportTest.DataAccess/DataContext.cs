﻿using AirportTest.Models;
using Microsoft.EntityFrameworkCore;

namespace AirportTest.DataAccess
{

    public class DataContext : DbContext
    {
        public DbSet<Airport> Airports { get; set; }
 

        public DataContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Airport>()
                .ToTable("Airports"); 
        }

    } 
}
