﻿using AirportTest.DataAccess.Interfaces;
using AirportTest.Models;

namespace AirportTest.DataAccess.Repositories
{
    public class AirportRepository : EntityBaseRepository<Airport>, IAirportRepository
    {
        public AirportRepository(DataContext context)
            : base(context)
        { }
    }
}
